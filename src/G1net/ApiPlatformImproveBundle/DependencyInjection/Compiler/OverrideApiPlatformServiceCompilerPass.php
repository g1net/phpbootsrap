<?php

namespace G1net\ApiPlatformImproveBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverrideApiPlatformServiceCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $documentNormalizer = $container->getDefinition('api_platform.swagger.normalizer.documentation');
        $metadataFactory = $container->getDefinition('g1net_api_platform_improve.metadata.route_param_metadata_factory');
        $documentNormalizer->replaceArgument(0, $metadataFactory);
    }

}
