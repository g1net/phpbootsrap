<?php

namespace G1net\ApiPlatformImproveBundle\Metadata;

use ApiPlatform\Core\Exception\ResourceClassNotFoundException;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\ResourceMetadata;

use Doctrine\Common\Annotations\Reader;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Route;

class RouteParamMetadataFactory implements ResourceMetadataFactoryInterface
{

    /**
     * @var Router
     */
    private $router;

    /**
     * @var ResourceMetadataFactoryInterface
     */
    private $decorated;

    /**
     * ActionResourceMetadataFactory constructor.
     * @param Router $router
     * @param ResourceMetadataFactoryInterface $decorated
     */
    public function __construct(Router $router, ResourceMetadataFactoryInterface $decorated)
    {
        $this->router = $router;
        $this->decorated = $decorated;
    }

    /**
     * Creates a resource metadata.
     *
     * @param string $resourceClass
     *
     * @throws ResourceClassNotFoundException
     *
     * @return ResourceMetadata
     */
    public function create(string $resourceClass): ResourceMetadata
    {
        $parentResourceMetadata = $this->decorated->create($resourceClass);
        $itemOperations = $parentResourceMetadata->getItemOperations();
        foreach ($itemOperations as $key => $operation) {

            if (!array_key_exists("route_name", $operation) ||
                (
                    array_key_exists("swagger_context", $operation) &&
                    array_key_exists("parameters", $operation['swagger_context'])
                )
            ) {
                continue;
            }
            /** @var Route $router */
            $router = $this->router->getRouteCollection()->get($operation['route_name']);
            if($router === null){
                continue;
            }
            $compiledRoute = $router->compile();
            $routeVariables = $compiledRoute->getVariables();

            if(!empty($routeVariables)){
                $parameters = [];
                foreach ($routeVariables as $param) {
                    $parameters[] = [
                        "name" => $param,
                        "in" => "path",
                        "required" => "true",
                        "type" => "string",
                    ];
                }
                $itemOperations[$key]["swagger_context"]['parameters'] = $parameters;
            }

        }

        return $parentResourceMetadata->withItemOperations($itemOperations);
    }
}