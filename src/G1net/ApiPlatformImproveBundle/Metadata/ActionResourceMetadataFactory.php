<?php
namespace G1net\ApiPlatformImproveBundle\Metadata;

use ApiPlatform\Core\Exception\ResourceClassNotFoundException;
use ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\ResourceMetadata;

use Doctrine\Common\Annotations\Reader;

use Symfony\Bundle\FrameworkBundle\Routing\Router;

use G1net\ApiPlatformImproveBundle\Annotation\Documentation;

class ActionResourceMetadataFactory implements ResourceMetadataFactoryInterface
{

    /**
     * @var Router
     */
    private $router;
    /**
     * @var Reader
     */
    private $reader;
    /**
     * @var ResourceMetadataFactoryInterface
     */
    private $decorated;
    /**
     * @var PropertyMetadataFactoryInterface
     */
    private $propertyMetadataFactory;

    /**
     * ActionResourceMetadataFactory constructor.
     * @param Reader $reader
     * @param Router $router
     * @param ResourceMetadataFactoryInterface $decorated
     * @param PropertyMetadataFactoryInterface $propertyMetadataFactory
     */
    public function __construct(
        Reader $reader,
        Router $router,
        ResourceMetadataFactoryInterface $decorated,
        PropertyMetadataFactoryInterface $propertyMetadataFactory
    )
    {
        $this->router = $router;
        $this->reader = $reader;
        $this->decorated = $decorated;
        $this->propertyMetadataFactory = $propertyMetadataFactory;
    }

    /**
     * Creates a resource metadata.
     *
     * @param string $resourceClass
     *
     * @throws ResourceClassNotFoundException
     *
     * @return ResourceMetadata
     */
    public function create(string $resourceClass): ResourceMetadata
    {
        $parentResourceMetadata = $this->decorated->create($resourceClass);
        $itemOperations = $parentResourceMetadata->getItemOperations();
        foreach ($itemOperations as $key => $operation) {

            if (!array_key_exists("route_name", $operation) || array_key_exists("swagger_context", $operation)) {
                continue;
            }

            $router = $this->router->getRouteCollection()->get($operation['route_name']);
            $object = explode('::', $router->getDefault('_controller'));
            $reflectionMethod = new \ReflectionMethod($object[0], $object[1]);
            /** @var Documentation $documentation */
            $documentation = $this->reader->getMethodAnnotation($reflectionMethod, Documentation::class);
            if ($documentation == null) {
                continue;
            }

            if ($documentation->summary !== null) {
                $itemOperations[$key]["swagger_context"]['summary'] = $documentation->summary;
            }
            if ($documentation->consumes !== null) {
                $itemOperations[$key]["swagger_context"]['consumes'] = $documentation->consumes;
            }
            if ($documentation->parameters !== null) {
                $itemOperations[$key]["swagger_context"]['parameters'] = $documentation->parameters;
            }
            if ($documentation->produces !== null) {
                $itemOperations[$key]["swagger_context"]['produces'] = $documentation->produces;
            }
            if ($documentation->responses !== null) {
                $itemOperations[$key]["swagger_context"]['responses'] = $documentation->responses;
            }
        }

        return $parentResourceMetadata->withItemOperations($itemOperations);
    }
}