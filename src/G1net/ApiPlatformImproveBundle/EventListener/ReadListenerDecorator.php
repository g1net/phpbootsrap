<?php

namespace G1net\ApiPlatformImproveBundle\EventListener;

use ApiPlatform\Core\EventListener\ReadListener;
use ApiPlatform\Core\Exception\RuntimeException;
use ApiPlatform\Core\Util\RequestAttributesExtractor;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Retrieves data from the applicable data provider and sets it as a request parameter called data.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ReadListenerDecorator
{

    private $defaultOpration = ['get', 'post', 'put', 'delete'];

    /**
     * @var ReadListener
     */
    private $readListener;

    public function __construct(ReadListener $readListener)
    {

        $this->readListener = $readListener;
    }

    /**
     * Calls the data provider and sets the data attribute.
     *
     * @param GetResponseEvent $event
     *
     * @throws NotFoundHttpException
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        try {
            $attributes = RequestAttributesExtractor::extractAttributes($request);
        } catch (RuntimeException $e) {
            return;
        }

        if(!$this->isDefaultOperation($attributes) && $request->attributes->get('id')== null){
            return;
        }

        return $this->readListener->onKernelRequest($event);
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function isDefaultOperation($attributes)
    {
        $operationName = $attributes['collection_operation_name'] ?? $attributes['item_operation_name'];

        return in_array($operationName, $this->defaultOpration);
    }

}
