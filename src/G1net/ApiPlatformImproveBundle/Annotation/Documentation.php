<?php

namespace G1net\ApiPlatformImproveBundle\Annotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
final class Documentation
{
    /**
     * @var string
     */
    public $summary;

    /**
     * @var array
     */
    public $parameters;

    /**
     * @var array
     */
    public $responses;

    /**
     * @var array
     */
    public $consumes;

    /**
     * @var array
     */
    public $produces;

}