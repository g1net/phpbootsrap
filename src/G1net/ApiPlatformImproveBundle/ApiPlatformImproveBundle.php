<?php

namespace G1net\ApiPlatformImproveBundle;

use G1net\ApiPlatformImproveBundle\DependencyInjection\Compiler\OverrideApiPlatformServiceCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ApiPlatformImproveBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new OverrideApiPlatformServiceCompilerPass());
    }

}
