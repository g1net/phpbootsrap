<?php

namespace AppBundle\Util;


use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use ApiPlatform\Core\Exception\RuntimeException;

class ResponseFactory
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var SerializerContextBuilderInterface
     */
    private $serializerContextBuilder;

    /**
     * ResponseFactory constructor.
     * @param SerializerInterface $serializer
     * @param SerializerContextBuilderInterface $serializerContextBuilder
     */
    public function __construct(SerializerInterface $serializer, SerializerContextBuilderInterface $serializerContextBuilder)
    {
        $this->serializer = $serializer;
        $this->serializerContextBuilder = $serializerContextBuilder;
    }

    /**
     * @param $data Object|array
     * @param Request $request
     * @param int $status
     * @return Response
     */
    public function create($data, Request $request, $status = 200){

        try {
            $attributes = RequestAttributesExtractor::extractAttributes($request);
            $context = $this->serializerContextBuilder->createFromRequest($request, true, $attributes);
        } catch (RuntimeException $e) {
            $context = [];
        }

        $response =  new Response(
            $this->serializer->serialize($data, $request->getRequestFormat(), $context),
            $status,
            [
                'Content-Type' => sprintf('%s; charset=utf-8', $request->getMimeType($request->getRequestFormat())),
                'Vary' => 'Accept',
                'X-Content-Type-Options' => 'nosniff',
                'X-Frame-Options' => 'deny',
            ]
        );

        return $response;
    }
}