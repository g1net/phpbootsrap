<?php

namespace AppBundle\Util;

use Symfony\Component\Translation\TranslatorInterface;

class LocaleSwitcher
{
    /** @var string */
    private $defaultLocale;

    /** @var TranslatorInterface */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function setLocale($locale = 'en_EN')
    {
        $currentLocale = $this->translator->getLocale();

        if ($locale === $currentLocale) {
            return;
        }

        $this->defaultLocale = $currentLocale;
        $this->translator->setLocale($locale);
    }

    public function restoreLocale()
    {
        $this->translator->setLocale($this->defaultLocale);
    }
}