<?php

namespace AppBundle\Util\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\MappingException;

/**
 * Class DefaultSortingRepository
 */
class DefaultSortingRepository extends EntityRepository
{

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return parent::findBy($criteria, $this->setupDefaultSorting($orderBy), $limit, $offset);
    }


    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return parent::findOneBy($criteria, $this->setupDefaultSorting($orderBy));
    }

    /**
     * @param $orderBy
     * @return array|null
     */
    protected function setupDefaultSorting($orderBy)
    {

        if ($orderBy === null) {
            try{
                $idName = $this->_class->getSingleIdentifierFieldName();
                if($this->_class->hasField($idName)){
                    $orderBy = [$idName => 'asc'];
                }
            } catch (MappingException $exception){ }
        }

        return $orderBy;
    }
}
