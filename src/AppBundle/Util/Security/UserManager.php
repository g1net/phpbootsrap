<?php

namespace AppBundle\Util\Security;


use Doctrine\ORM\EntityManager;

use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;

/**
 * Class UserManager
 */
class UserManager
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var PasswordUpdater
     */
    private $passwordUpdater;

    /**
     * @var CanonicalFieldsUpdater
     */
    private $canonicalFieldsUpdater;

    /**
     * @var EntityManager
     */
    private $objectManager;

    /**
     * @var boolean
     */
    private $confirmation;

    /**
     * UserManager constructor.
     * @param UserRepository $userRepository
     * @param PasswordUpdater $passwordUpdater
     * @param CanonicalFieldsUpdater $canonicalFieldsUpdater
     * @param $confirmation
     */
    public function __construct(
        UserRepository $userRepository,
        PasswordUpdater $passwordUpdater,
        CanonicalFieldsUpdater $canonicalFieldsUpdater,
        bool $confirmation
    )
    {
        $this->objectManager = $userRepository->getObjectManager();
        $this->userRepository = $userRepository;
        $this->passwordUpdater = $passwordUpdater;
        $this->canonicalFieldsUpdater = $canonicalFieldsUpdater;
        $this->confirmation = $confirmation;
    }

    /**
     * @param array $criteria
     * @return null|object
     */
    public function findUserBy(array $criteria)
    {
        return $this->userRepository->findOneBy($criteria);
    }

    /**
     * @param User $user
     */
    public function deleteUser(User $user)
    {
        $user->setEnabled(false);
        $user->setDeleted(true);
        $user->setPassword(uniqid('deleted'));
        $user->setEmailCanonical(uniqid('deleted'));
        $user->setResetPasswordToken(null);
        $user->setConfirmToken(null);

        $this->objectManager->persist($user);
        $this->objectManager->flush();
    }

    /**
     * @param User $user
     */
    public function updateUser(User $user)
    {
        $this->updateCanonicalFields($user);
        $this->updatePassword($user);

        $this->objectManager->persist($user);
        $this->objectManager->flush();
    }

    /**
     * @param User $user
     */
    public function updatePassword(User $user)
    {
        $this->passwordUpdater->hashPassword($user);
    }

    /**
     * @param User $user
     */
    public function updateCanonicalFields(User $user)
    {
        $this->canonicalFieldsUpdater->updateCanonicalFields($user);
    }

    /**
     * @param User $user
     */
    public function generateResetPasswordToken(User $user)
    {
        $user->setResetPasswordToken(TokenGenerator::generate());
    }

    /**
     * @param User $user
     */
    public function setupIsActivationRequired(User $user)
    {
        if ($this->confirmation == false) {
            $user->setEnabled('true');
        }
    }

    public function generateConfirmationToken(User $user)
    {
        $user->setConfirmToken(TokenGenerator::generate());
    }

}