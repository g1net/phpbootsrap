<?php

namespace AppBundle\Util\Security;


class TokenGenerator
{
    /**
     * @return string
     */
    public static function generate()
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $randomString = openssl_random_pseudo_bytes(32);
        } else {
            $randomString = hash('sha256', uniqid(mt_rand(), true), true);
        }

        return rtrim(strtr(base64_encode($randomString), '+/', '-_'), '=');
    }
}