<?php

namespace AppBundle\Util\Security;


use AppBundle\Entity\User;

/**
 * Class CanonicalFieldsUpdater
 */
class CanonicalFieldsUpdater
{

    /**
     * @param User $user
     */
    public function updateCanonicalFields(User $user)
    {
        $user->setEmailCanonical($this->canonicalizeEmail($user->getEmail()));
    }


    /**
     * @param $email
     * @return null|string
     */
    public function canonicalizeEmail($email)
    {
        return $this->canonicalize($email);
    }

    /**
     * @param $string
     * @return null|string
     */
    private function canonicalize($string)
    {
        if (null === $string) {
            return null;
        }

        $encoding = mb_detect_encoding($string);
        $result = $encoding
            ? mb_convert_case($string, MB_CASE_LOWER, $encoding)
            : mb_convert_case($string, MB_CASE_LOWER);

        return $result;
    }
}
