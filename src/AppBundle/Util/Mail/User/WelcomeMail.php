<?php
namespace AppBundle\Util\Mail\User;

use Symfony\Bundle\TwigBundle\TwigEngine;

use AppBundle\Entity\Oauth\Client;
use AppBundle\Entity\User;
use AppBundle\Util\Mail\Mailer;
use AppBundle\Util\LocaleSwitcher;


class WelcomeMail
{
    const TEMPLATE_SUBJECT = '@App/mail/user_welcome_subject.txt.twig';
    const TEMPLATE_BODY = '@App/mail/user_welcome_body.txt.twig';

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $from;

    /**
     * @var TwigEngine
     */
    private $twigEngine;
    /**
     * @var LocaleSwitcher
     */
    private $localeSwitcher;

    public function __construct(TwigEngine $twigEngine, Mailer $mailer, LocaleSwitcher $localeSwitcher, string $from)
    {
        $this->twigEngine = $twigEngine;
        $this->mailer = $mailer;
        $this->localeSwitcher = $localeSwitcher;
        $this->from = $from;
    }


    public function send(User $user, Client $client)
    {
        $parameters = [
            'email' => $user->getEmail(),
            'url' => str_replace('{token}', $user->getConfirmToken(), $client->getConfirmUserEmailUrlPattern()),
        ];
        $this->localeSwitcher->setLocale();
        $subject = $this->twigEngine->render(self::TEMPLATE_SUBJECT, $parameters);
        $body = $this->twigEngine->render(self::TEMPLATE_BODY, $parameters);
        $this->localeSwitcher->restoreLocale();

        return $this->mailer->sendMail($subject, $body, $user->getEmail(), $this->from);

    }

}