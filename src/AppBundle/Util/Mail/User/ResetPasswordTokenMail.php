<?php
namespace AppBundle\Util\Mail\User;

use Symfony\Bundle\TwigBundle\TwigEngine;

use AppBundle\Entity\Oauth\Client;
use AppBundle\Entity\User;
use AppBundle\Util\Mail\Mailer;
use AppBundle\Util\LocaleSwitcher;


class ResetPasswordTokenMail
{
    const TEMPLATE_SUBJECT = '@App/mail/reset_password_token_subject.txt.twig';
    const TEMPLATE_BODY = '@App/mail/reset_password_token_body.txt.twig';

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $from;

    /**
     * @var TwigEngine
     */
    private $twigEngine;
    /**
     * @var LocaleSwitcher
     */
    private $localeSwitcher;

    public function __construct(TwigEngine $twigEngine, Mailer $mailer, LocaleSwitcher $localeSwitcher, string $from)
    {
        $this->twigEngine = $twigEngine;
        $this->mailer = $mailer;
        $this->localeSwitcher = $localeSwitcher;
        $this->from = $from;
    }


    public function send(User $user, Client $client)
    {
        $parameters = [
            'url' => str_replace('{token}', $user->getResetPasswordToken(), $client->getResetUserPasswordUrlPattern()),
            'email' => $user->getEmail(),
        ];

        $this->localeSwitcher->setLocale();
        $subject = $this->twigEngine->render(self::TEMPLATE_SUBJECT, $parameters);
        $body = $this->twigEngine->render(self::TEMPLATE_BODY, $parameters);
        $this->localeSwitcher->restoreLocale();

        return $this->mailer->sendMail($subject, $body, $user->getEmail(), $this->from);

    }

}