<?php
namespace AppBundle\Util\Mail;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class Mailer
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendMail($subject, $body, $recipientEmail, $senderEmail){

        $message = \Swift_Message::newInstance($subject, $body)
            ->setFrom($senderEmail)
            ->setTo($recipientEmail);

        $sentEmails = $this->mailer->send($message);

        return (bool) $sentEmails;
    }
}