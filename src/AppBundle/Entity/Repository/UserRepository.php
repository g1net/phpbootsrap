<?php

namespace AppBundle\Entity\Repository;


use AppBundle\Util\Repository\DefaultSortingRepository;

class UserRepository extends DefaultSortingRepository
{
    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getObjectManager(){
       return $this->getEntityManager();
    }
}