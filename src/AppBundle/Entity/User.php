<?php
namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 * @UniqueEntity("emailCanonical", errorPath="email", groups={"user-create"})
 *
 * @ApiResource(
 *      collectionOperations={
 *          "get"={"method"="GET"},
 *          "create_new_user"={
 *              "route_name"="create_new_user",
 *              "denormalization_context"={"groups"={"user-create"}},
 *              "validation_groups" = {"user-create"}
 *          }
 *      },
 *      itemOperations={
 *          "get"={"method" = "GET"},
 *          "put"={"method" = "PUT"},
 *          "delete"={"method" = "DELETE"},
 *          "send_reset_password_token" = {
 *              "route_name"="send_reset_password_token",
 *              "normalization_context" = {"groups" = {"user-reset-password-read"}},
 *              "denormalization_context" = {"groups" = {"user-reset-password-write"}}
 *           },
 *           "reset_password" = {
 *              "route_name"="reset_password",
 *              "normalization_context" = {"groups" = {"user-reset-password-read"}},
 *              "denormalization_context" = {"groups" = {"user-reset-password-write"}},
 *              "validation_groups" = {"user-reset-password-write"},
 *           },
 *           "confirm_password" = {
 *              "route_name"="confirm_email",
 *              "normalization_context" = {"groups" = {"user-reset-password-read"}},
 *              "denormalization_context" = {"groups" = {"user-confirm-email-write"}},
 *              "validation_groups" = {"user-confirm-email-write"}
 *           }
 *      },
 *     attributes={
 *          "normalization_context" = {"groups" = {"user-read"}},
 *          "denormalization_context" = {"groups" = {"user-write"}}
 *      }
 * )
 */
class User implements AdvancedUserInterface
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"user-read"})
     */
    private $id;

    /**
     * @var string $email The email of the user.
     *
     * @Assert\Email(groups={"user-create"})
     * @Assert\NotBlank(groups={"user-create"})
     *
     * @Groups({"user-read", "user-create", "user-reset-password-read"})
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var string $email The email of the user.
     * @ORM\Column(type="string")
     */
    private $emailCanonical;

    /**
     * @var string Plain password. Used for model validation. Must not be persisted.
     *
     * @Assert\Length(min = 5, groups={"user-create", "user-reset-password-write"})
     * @Assert\NotBlank(groups={"user-create", "user-reset-password-write"})
     * @Groups({"user-create", "user-reset-password-write"})
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string")
     */
    private $salt;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Groups({"user-read"})
     */
    private $enabled;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Groups({"user-read"})
     */
    private $confirmed;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Groups({"user-read"})
     */
    private $deleted;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $confirmToken;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $resetPasswordToken;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     * @ORM\Column( type="string")
     */
    private $password;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     * @Assert\NotBlank(groups={"user-create"})
     * @Assert\Choice(callback={"\AppBundle\Enum\User\UserRoleEnum", "getValues"}, multiple=true, groups={"user-create"})
     * @Groups({"user-read", "user-create"})
     */
    private $roles;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->setSalt(md5(time()));
        $this->setDeleted(false);
        $this->setEnabled(false);
        $this->setConfirmed(false);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    /**
     * @param string $emailCanonical
     * @return User
     */
    public function setEmailCanonical(string $emailCanonical = null)
    {
        $this->emailCanonical = $emailCanonical;
        return $this;
    }


    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     * @return User
     */
    public function setPlainPassword(string $plainPassword)
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     * @return User
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @param boolean $confirmed
     * @return User
     */
    public function setConfirmed(bool $confirmed)
    {
        $this->confirmed = $confirmed;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param boolean $deleted
     * @return User
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmToken()
    {
        return $this->confirmToken;
    }

    /**
     * @param string $confirmToken
     * @return User
     */
    public function setConfirmToken($confirmToken)
    {
        $this->confirmToken = $confirmToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getResetPasswordToken()
    {
        return $this->resetPasswordToken;
    }

    /**
     * @param string $resetPasswordToken
     * @return User
     */
    public function setResetPasswordToken($resetPasswordToken)
    {
        $this->resetPasswordToken = $resetPasswordToken;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired()
    {
        if($this->isDeleted()){
            return false;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        $this->getEmail();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null ;
    }
}
