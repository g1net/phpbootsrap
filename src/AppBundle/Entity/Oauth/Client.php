<?php


namespace AppBundle\Entity\Oauth;

use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ClientRepository")
 */
class Client extends BaseClient
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=20, nullable=true, unique=true)
     */
    protected $shortName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $confirmUserEmailUrlPattern;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $resetUserPasswordUrlPattern;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getConfirmUserEmailUrlPattern()
    {
        return $this->confirmUserEmailUrlPattern;
    }

    /**
     * @param mixed $confirmUserEmailUrlPattern
     * @return Client
     */
    public function setConfirmUserEmailUrlPattern($confirmUserEmailUrlPattern)
    {
        $this->confirmUserEmailUrlPattern = $confirmUserEmailUrlPattern;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResetUserPasswordUrlPattern()
    {
        return $this->resetUserPasswordUrlPattern;
    }

    /**
     * @param mixed $resetUserPasswordUrlPattern
     * @return Client
     */
    public function setResetUserPasswordUrlPattern($resetUserPasswordUrlPattern)
    {
        $this->resetUserPasswordUrlPattern = $resetUserPasswordUrlPattern;

        return $this;
    }

}