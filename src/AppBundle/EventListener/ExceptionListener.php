<?php


namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Handles requests errors.
 *
 * @author Samuel ROZE <samuel.roze@gmail.com>
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
final class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        //hide param convert not found exceptions
        $exception = $event->getException();
        if($exception instanceof NotFoundHttpException && $exception->getMessage() != "Not found"){
            $event->setException(new NotFoundHttpException("Not found"), $event->getException());
        }
    }
}
