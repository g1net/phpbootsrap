<?php

namespace AppBundle\Action\User;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;

use AppBundle\Entity\User;

/**
 * Class ConfirmEmail
 * @package AppBundle\Action\User
 */
class ConfirmEmail
{
    const ACTION = 'confirm_email';

    /**
     * @Route(
     *     name="confirm_email",
     *     path="/users/confirm-email/{confirmToken}",
     *     defaults={
     *          "_api_item_operation_name"="confirm_email",
     *          "_api_resource_class"=User::class
     *     }
     * )
     * @Method("PUT")
     * @Documentation(
     *      summary="Confirm user email",
     * )
     * @param User $user
     * @return User
     */
    public function __invoke(User $user, User $data)
    {
        $user->setEnabled(true);
        $user->setConfirmed(true);
        $user->setConfirmToken(null);

        return $user;
    }

}