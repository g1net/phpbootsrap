<?php

namespace AppBundle\Action\User;

use AppBundle\Util\Security\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use AppBundle\Entity\Oauth\Client;
use AppBundle\Entity\User;

class CreateNewUser
{
    const ACTION = 'create_new_user';

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * CreateNewUser constructor.
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @Route(
     *     name="create_new_user",
     *     path="/users/{apiShortName}",
     *     defaults={"_api_collection_operation_name"="create_new_user", "_api_resource_class"=User::class}
     * )
     * @Method("POST")
     *
     * @ParamConverter("client", options={"mapping": {"apiShortName": "shortName"}})
     *
     * @param Request $request
     * @param User $data
     * @param Client $client
     * @return Response
     */
    public function __invoke(Request $request, User $data, Client $client)
    {
        $this->userManager->generateConfirmationToken($data);
        $this->userManager->setupIsActivationRequired($data);

        return $data;
    }

}