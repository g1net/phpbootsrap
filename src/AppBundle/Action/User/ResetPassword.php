<?php

namespace AppBundle\Action\User;


use G1net\ApiPlatformImproveBundle\Annotation\Documentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;

use AppBundle\Entity\User;

class ResetPassword
{
    const ACTION = 'reset_password';

    /**
     * @Route(
     *     name="reset_password",
     *     path="/users/reset-password/{resetPasswordToken}",
     *     defaults={"_api_item_operation_name"="reset_password", "_api_resource_class"=User::class, }
     * )
     * @Method("PUT")
     *
     * @Documentation(
     *      summary="Set new password by reset password token",
     * )
     *
     *
     * @param User $user reset password token
     * @return User
     */
    public function __invoke(User $user, User $data)
    {
        $user->setPlainPassword($data->getPlainPassword());
        $user->setResetPasswordToken(null);

        return $user;
    }

}