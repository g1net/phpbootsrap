<?php

namespace AppBundle\Action\User;

use G1net\ApiPlatformImproveBundle\Annotation\Documentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use AppBundle\Entity\Oauth\Client;
use AppBundle\Entity\User;
use AppBundle\Util\Security\UserManager;

class SendResetPasswordToken
{
    const ACTION = 'send_reset_password_token';

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * SendResetPasswordToken constructor.
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @Route(
     *     name="send_reset_password_token",
     *     path="/users/send-reset-password-token/{apiShortName}/{email}",
     *     defaults={"_api_item_operation_name"="send_reset_password_token", "_api_resource_class"=User::class}
     * )
     * @Method("POST")
     *
     * @ParamConverter("client", options={"mapping": {"apiShortName": "shortName"}})
     *
     * @Documentation(
     *      summary="Send reset password token",
     * )
     *
     * @param Request $request
     * @param Client $client
     * @param User $user
     * @return Response
     */
    public function __invoke(Request $request, Client $client, User $user)
    {
        $this->userManager->generateResetPasswordToken($user);

        return $user;
    }

}