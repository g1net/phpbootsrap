<?php

namespace AppBundle\EventSubscriber;


use ApiPlatform\Core\Exception\RuntimeException;
use ApiPlatform\Core\Util\RequestAttributesExtractor;

abstract class ApiResourceSubscriberInterface
{
    const ACTION_CREATE = 'post';
    const ACTION_UPDATE = 'put';
    const ACTION_GET = 'get';
    const ACTION_DELETE = 'delete';

    /**
     * @param $request
     * @return null|string
     */
    protected function getOperationName($request)
    {
        $attributes = $this->getAttributes($request);

        return $attributes['collection_operation_name'] ?? $attributes['item_operation_name'] ?? null;

    }

    /**
     * @param $request
     * @return bool
     */
    protected function isCollectionOperation($request)
    {
        $attributes = $this->getAttributes($request);
        return array_key_exists('collection_operation_name', $attributes);
    }

    /**
     * @param $request
     * @return bool
     */
    protected function isItemOperation($request)
    {
        $attributes = $this->getAttributes($request);
        return array_key_exists('item_operation_name', $attributes);
    }

    /**
     * @param $request
     * @return array|void
     */
    private function getAttributes($request)
    {
        try {
            return RequestAttributesExtractor::extractAttributes($request);
        } catch (RuntimeException $e) {
            return;
        }
    }
}