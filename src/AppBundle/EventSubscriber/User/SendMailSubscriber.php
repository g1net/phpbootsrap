<?php

namespace AppBundle\EventSubscriber\User;


use ApiPlatform\Core\EventListener\EventPriorities;
use AppBundle\Action\User\CreateNewUser;
use AppBundle\Action\User\SendResetPasswordToken;
use AppBundle\Entity\Oauth\Client;
use AppBundle\Entity\User;
use AppBundle\EventSubscriber\ApiResourceSubscriberInterface;
use AppBundle\Util\Mail\User\ResetPasswordTokenMail;
use AppBundle\Util\Mail\User\WelcomeMail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class SendMailSubscriber extends ApiResourceSubscriberInterface implements EventSubscriberInterface
{

    private $mailer;

    /**
     * @var ResetPasswordTokenMail
     */
    private $resetPasswordTokenMail;

    /**
     * @var WelcomeMail
     */
    private $welcomeMail;

    public function __construct(
        \Swift_Mailer $mailer,
        ResetPasswordTokenMail $resetPasswordTokenMail,
        WelcomeMail $welcomeMail
    )
    {
        $this->mailer = $mailer;
        $this->resetPasswordTokenMail = $resetPasswordTokenMail;
        $this->welcomeMail = $welcomeMail;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                ['sendConfirmationEmail', EventPriorities::POST_WRITE],
                ['sendResetUserPasswordEmail', EventPriorities::POST_WRITE]
            ],
        ];
    }

    public function sendConfirmationEmail(GetResponseForControllerResultEvent $event)
    {
        $request = $event->getRequest();
        $user = $event->getControllerResult();
        $client = $request->attributes->get('client');

        if (!$user instanceof User || !$client instanceof Client
            || Request::METHOD_POST !== $request->getMethod()
            || $this->getOperationName($request) !== CreateNewUser::ACTION
            || !$this->isCollectionOperation($request)
        ) {
            return;
        }

        if ($user->isEnabled()) {
            $this->welcomeMail->send($user, $client);
        } else {
            $this->activateMail->send($user, $client);
        }

    }

    public function sendResetUserPasswordEmail(GetResponseForControllerResultEvent $event)
    {
        $request = $event->getRequest();
        $user = $event->getControllerResult();
        $client = $request->attributes->get('client');

        if (!$user instanceof User || !$client instanceof Client
            || Request::METHOD_POST !== $request->getMethod()
            || $this->getOperationName($request) !== SendResetPasswordToken::ACTION
            || !$this->isItemOperation($request)
        ) {
            return;
        }

        $this->resetPasswordTokenMail->send($user, $client);
    }

}