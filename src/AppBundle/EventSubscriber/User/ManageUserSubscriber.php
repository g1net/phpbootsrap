<?php

namespace AppBundle\EventSubscriber\User;

use ApiPlatform\Core\EventListener\EventPriorities;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

use AppBundle\EventSubscriber\ApiResourceSubscriberInterface;
use AppBundle\Entity\User;
use AppBundle\Util\Security\UserManager;

class ManageUserSubscriber extends ApiResourceSubscriberInterface implements EventSubscriberInterface
{

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * UserSubscriber constructor.
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                ['manageUser', EventPriorities::PRE_WRITE],
                ['updateUserCanonicalFields', EventPriorities::PRE_VALIDATE],
            ],
        ];
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function manageUser(GetResponseForControllerResultEvent $event)
    {
        $request = $event->getRequest();
        $user = $event->getControllerResult();

        if (!$user instanceof User || $request->isMethodSafe()) {
            return;
        }

        switch ($request->getMethod()) {
            case Request::METHOD_DELETE:
                $this->userManager->deleteUser($user);
                $event->setControllerResult(null);
                break;

            default:
                $this->userManager->updateUser($user);
                break;
        }
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function updateUserCanonicalFields(GetResponseForControllerResultEvent $event)
    {
        $request = $event->getRequest();
        $user = $event->getControllerResult();

        if (!$user instanceof User || $request->isMethodSafe()) {
            return;
        }

        $this->userManager->updateCanonicalFields($user);
    }
}
