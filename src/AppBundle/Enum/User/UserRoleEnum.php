<?php

namespace AppBundle\Enum\User;

class UserRoleEnum
{
    const SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    const ADMIN = 'ROLE_ADMIN';
    const USER = 'ROLE_USER';

    /**
     * @return array
     */
    public static function getValues()
    {
        return [
            self::SUPER_ADMIN,
            self::ADMIN,
            self::USER,
        ];
    }

}