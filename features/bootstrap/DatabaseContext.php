<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Hook\Scope\BeforeFeatureScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\SchemaTool;

/**
 * Defines application features from the specific context.
 */
class DatabaseContext implements Context
{

    /**
     * @var array|null
     */
    private static $resetDatabaseSql;

    /**
     * @var array|null
     */
    private static $cachedObject;

    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $manager;

    /**
     * @var SchemaTool
     */
    private $schemaTool;

    /**
     * @var array
     */
    private $classes;
    /**
     * @var
     */
    private $fixtureFiles;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     * @param ManagerRegistry $doctrine
     * @param $fixtures
     */
    public function __construct(ManagerRegistry $doctrine, $fixtures)
    {

        $this->doctrine = $doctrine;
        $this->manager = $doctrine->getManager();
        $this->schemaTool = new SchemaTool($this->manager);
        $this->classes = $this->manager->getMetadataFactory()->getAllMetadata();
        $this->fixtureFiles = $fixtures;
    }

    /** @BeforeScenario */
    public function BeforeScenario()
    {
            $this->createDatabase();
            $this->loadFixtures();
    }

    /**
     * @BeforeScenario @resetDatabase
     */
    public function resetDatabase()
    {
        self::$resetDatabaseSql = null;
        self::$cachedObject = null;
        $this->BeforeScenario();
    }


    private function createDatabase()
    {
        if (self::$resetDatabaseSql == null) {
            $sql = $this->schemaTool->getDropSchemaSQL($this->classes);
            $this->executeQuery($sql);

            $sqlCreate = $this->schemaTool->getCreateSchemaSql($this->classes);
            $this->executeQuery($sqlCreate);

            self::$resetDatabaseSql = array_merge($this->schemaTool->getDropSchemaSQL($this->classes), $sqlCreate);
        } else {
            $this->executeQuery(self::$resetDatabaseSql);
        }
    }

    private function loadFixtures(){
        if(self::$cachedObject == null){
            self::$cachedObject = [];
            $loader = new \Nelmio\Alice\Fixtures\Loader();
            foreach($this->fixtureFiles as $file){
                self::$cachedObject = array_merge(self::$cachedObject , $loader->load(__DIR__.'/../../'.$file));
            }
        }
        $persister = new \Nelmio\Alice\Persister\Doctrine($this->manager);
        $persister->persist( self::$cachedObject);
    }

    private function executeQuery(array $sql)
    {
        $conn = $this->manager->getConnection();
        foreach ($sql as $query) {
            try {
                $conn->executeQuery($query);
            } catch (Exception $ex) {
            }
        }
    }
}
