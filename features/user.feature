Feature: User
  In order have user accont
  As an API client
  I need to be able manage users

  @enableProfiler
  Scenario: Create new user
    And I send a "POST" request to "/users/oauth-client" with body:
    """
    {
      "email": "dummy@test.dev",
      "plainPassword": "testtest",
      "roles": [
        "ROLE_USER"
      ]
    }
    """
    Then the response status code should be 201
    And the response should contain json:
    """
    {
      "@context": "/contexts/User",
      "@id": "@string@",
      "@type": "User",
      "id": "@integer@",
      "email": "dummy@test.dev",
      "enabled": true,
      "confirmed": false,
      "deleted": false,
      "roles": [
        "ROLE_USER"
      ]
    }
    """
    And should sent "1" email
    And email with subject "Welcome dummy@test.dev!" should be sent to "dummy@test.dev"

  Scenario: Should not be able to create new user if email is already used
    And I send a "POST" request to "/users/oauth-client" with body:
    """
    {
      "email": "admin@test.dev",
      "plainPassword": "testtest",
      "roles": [
        "ROLE_USER"
      ]
    }
    """
    Then the response status code should be 400
    And the response should contain json:
    """
    {
      "@context": "/contexts/ConstraintViolationList",
      "@type": "ConstraintViolationList",
      "hydra:title": "An error occurred",
      "hydra:description": "@string@",
      "violations": [
        {
          "propertyPath": "email",
          "message": "@string@"
        }
      ]
    }
    """

  Scenario: Should not be able to create new user with out email, password and role field
    And I send a "POST" request to "/users/oauth-client" with body:
    """
    {}
    """
    Then the response status code should be 400
    And the response should contain json:
    """
    {
      "@context": "/contexts/ConstraintViolationList",
      "@type": "ConstraintViolationList",
      "hydra:title": "An error occurred",
      "hydra:description": "@string@",
      "violations": [
        {
          "propertyPath": "email",
          "message": "@string@"
        },
        {
          "propertyPath": "plainPassword",
          "message": "@string@"
        },
        {
          "propertyPath": "roles",
          "message": "@string@"
        }
      ]
    }
    """

  Scenario: A User should not be able to login if account is not active
    When I send a "POST" request to "/oauth/v2/token" with body:
    """
    {
      "grant_type": "password",
      "client_id": "1_uNLt2JVV5VLfpDmgJ8wytNEBe4KLEMwHuUwLLFTnQHJ5nKSQ",
      "client_secret":"z8LykkWGaXCeMD6sLSn3CzvPGHYrE3bxp3rZGPdVAL9uA2wB",
      "username": "user3@test.dev",
      "password": "pgx123!"
    }
    """
    Then the response status code should be 400

  Scenario: A User should be able to confirm email and activate account
    When I send a "PUT" request to "/users/confirm-email/confirmEmailToken"
    Then the response status code should be 200
    When I send a "POST" request to "/oauth/v2/token" with body:
    """
    {
      "grant_type": "password",
      "client_id": "1_uNLt2JVV5VLfpDmgJ8wytNEBe4KLEMwHuUwLLFTnQHJ5nKSQ",
      "client_secret":"z8LykkWGaXCeMD6sLSn3CzvPGHYrE3bxp3rZGPdVAL9uA2wB",
      "username": "user3@test.dev",
      "password": "pgx123!"
    }
    """
    Then the response status code should be 200

  @enableProfiler
  Scenario: A User should be able to get an email containing the url to set a new password
    When I send a "POST" request to "/users/send-reset-password-token/oauth-client/admin@test.dev"
    Then the response status code should be 201
    Then should sent "1" email
    And email with subject "Reset Password" should be sent to "admin@test.dev"
    And the response should contain json:
    """
    {
      "@context": "/contexts/User",
      "@id": "@string@",
      "@type": "User",
      "email": "admin@test.dev"
    }
    """

  Scenario: A User should be able to set a new password if reset password token is valid
    When I send a "PUT" request to "/users/reset-password/resetPasswordToken" with body:
    """
    {
      "plainPassword": "noweHaslo"
    }
    """
    Then the response status code should be 200
    When I send a "POST" request to "/oauth/v2/token" with body:
    """
    {
      "grant_type": "password",
      "client_id": "1_uNLt2JVV5VLfpDmgJ8wytNEBe4KLEMwHuUwLLFTnQHJ5nKSQ",
      "client_secret":"z8LykkWGaXCeMD6sLSn3CzvPGHYrE3bxp3rZGPdVAL9uA2wB",
      "username": "user2@test.dev",
      "password": "noweHaslo"
    }
    """
    Then the response status code should be 200

  Scenario: A User should not be able to set a new password if reset password token is invalid
    When I send a "PUT" request to "/users/reset-password/invalidResetPasswordToken" with body:
    """
    {
      "plainPassword": "noweHaslo"
    }
    """
    Then the response status code should be 404

  Scenario: Delete a user. After being deleted the user should not be able to log in or perform any action which requires access token.
    When I send a "DELETE" request to "/users/1"
    Then the response code should be 204
    And I send a "POST" request to "/oauth/v2/token" with body:
    """
    {
      "grant_type": "password",
      "client_id": "1_uNLt2JVV5VLfpDmgJ8wytNEBe4KLEMwHuUwLLFTnQHJ5nKSQ",
      "client_secret":"z8LykkWGaXCeMD6sLSn3CzvPGHYrE3bxp3rZGPdVAL9uA2wB",
      "username": "admin@test.dev",
      "password": "pgx123!"
    }
    """
    And the response status code should be 400