Feature: OAuth2 Token Grant Password

  Scenario: Without user credentials
    When I send a "POST" request to "/oauth/v2/token" with body:
    """
    {
      "grant_type": "password",
      "client_id": "1_uNLt2JVV5VLfpDmgJ8wytNEBe4KLEMwHuUwLLFTnQHJ5nKSQ",
      "client_secret":"z8LykkWGaXCeMD6sLSn3CzvPGHYrE3bxp3rZGPdVAL9uA2wB"
    }
    """
    Then the response status code should be 400
    And the response should contain json:
    """
    {
      "error": "invalid_request",
      "error_description": "Missing parameters. \"username\" and \"password\" required"
    }
    """

  Scenario: Invalid user credentials
    When I send a "POST" request to "/oauth/v2/token" with body:
    """
    {
      "grant_type": "password",
      "client_id": "1_uNLt2JVV5VLfpDmgJ8wytNEBe4KLEMwHuUwLLFTnQHJ5nKSQ",
      "client_secret":"z8LykkWGaXCeMD6sLSn3CzvPGHYrE3bxp3rZGPdVAL9uA2wB",
      "username": "invalidUser",
      "password": "invalidPass"
    }
    """
    Then the response status code should be 400
    And the response should contain json:
    """
    {
      "error": "invalid_grant",
      "error_description": "Invalid username and password combination"
    }
    """

  Scenario: Token Granted
    When I send a "POST" request to "/oauth/v2/token" with body:
    """
    {
      "grant_type": "password",
      "client_id": "1_uNLt2JVV5VLfpDmgJ8wytNEBe4KLEMwHuUwLLFTnQHJ5nKSQ",
      "client_secret":"z8LykkWGaXCeMD6sLSn3CzvPGHYrE3bxp3rZGPdVAL9uA2wB",
      "username": "admin@test.dev",
      "password": "pgx123!"
    }
    """
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "access_token": "@string@",
      "expires_in": 3600,
      "token_type": "bearer",
      "scope": "employee client",
      "refresh_token": "@string@"
    }
    """

  Scenario: Without Refresh Token
    When I send a "POST" request to "/oauth/v2/token" with body:
    """
    {
      "grant_type": "refresh_token",
      "client_id": "1_uNLt2JVV5VLfpDmgJ8wytNEBe4KLEMwHuUwLLFTnQHJ5nKSQ",
      "client_secret":"z8LykkWGaXCeMD6sLSn3CzvPGHYrE3bxp3rZGPdVAL9uA2wB"
    }
    """
    Then the response status code should be 400
    And the response should contain json:
    """
    {
      "error": "invalid_request",
      "error_description": "No \"refresh_token\" parameter found"
    }
    """

  Scenario: Invalid refresh token
    When I send a "POST" request to "/oauth/v2/token" with body:
    """
    {
      "grant_type": "refresh_token",
      "client_id": "1_uNLt2JVV5VLfpDmgJ8wytNEBe4KLEMwHuUwLLFTnQHJ5nKSQ",
      "client_secret":"z8LykkWGaXCeMD6sLSn3CzvPGHYrE3bxp3rZGPdVAL9uA2wB",
      "refresh_token": "invalidRefreshToken"
    }
    """
    Then the response status code should be 400
    And the response should contain json:
    """
    {
      "error": "invalid_grant",
      "error_description": "Invalid refresh token"
    }
    """
  Scenario: Refreshing OK
    When I send a "POST" request to "/oauth/v2/token" with body:
    """
    {
      "grant_type": "refresh_token",
      "client_id": "1_uNLt2JVV5VLfpDmgJ8wytNEBe4KLEMwHuUwLLFTnQHJ5nKSQ",
      "client_secret":"z8LykkWGaXCeMD6sLSn3CzvPGHYrE3bxp3rZGPdVAL9uA2wB",
      "refresh_token": "refresh_admin"
    }
    """
    Then the response status code should be 200
    And the response should contain json:
    """
    {
      "access_token": "@string@",
      "expires_in": 3600,
      "token_type": "bearer",
      "scope": "employee client",
      "refresh_token": "@string@"
    }
    """
